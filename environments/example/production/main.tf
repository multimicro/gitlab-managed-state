terraform {
  required_providers {
    nginx = {
      source = "getstackhead/nginx"
      version = "1.3.2"
    }
  }
}

provider "nginx" {
  # Configuration options
}
